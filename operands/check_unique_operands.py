# 03/14/2018
#check the unique operands for a set of operands 
import random 

#read global file
input_list = [] 
for input_file_name in ['conv2d_MUL_op_layer1_CNN', 'conv2d_MUL_op_layer2_CNN', 'conv2d_MUL_op_layer3_CNN', 'fully_MUL_op_CNN.txt']:
    input_file = open(input_file_name)
    input_list.extend(input_file.readlines())
random.shuffle(input_list) 

#check unique operands 
for i in [10000, 100000, 1000000, 10000000, 100000000]:
    unique_operands = len(set(input_list[:i]))
    print "percent of unique operands:", unique_operands/float(i)

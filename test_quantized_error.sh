#for error in 0.0000001; do
#for error in 0.5 0.2 0.1 0.05 0.02 0.01 0.005 0.002 0.001; do
#for error in 0.01 0.001 0.0001 0.00001 0.000001 0.0000001 0.00000001; do
for error in 0.07359; do 
	
	echo $error
	g++ -c test_quantized_error.cpp -I./ -pthread -std=c++11 -DCNN_USE_GEMMLOWP=true -DCNN_REGISTER_LAYER_DESERIALIZER=true -DADDER_ERROR_RATE=${error} -DMULTI_ERROR_RATE=0.116255
	g++ test_quantized_error.o -o test_quantized_error -pthread -std=c++11 -DCNN_USE_GEMMLOWP=true -DCNN_REGISTER_LAYER_DESERIALIZER=true -DADDER_ERROR_RATE=${error} -DMULTI_ERROR_RATE=0.116255

	./test_quantized_error examples/mnist 2>&1 | tee ${error}.log.test_quantize_error
done

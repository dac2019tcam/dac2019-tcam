import matplotlib.pyplot as plt
import numpy as np
import csv
degree_sign= u'\N{DEGREE SIGN}'

MLP_accu_loss = []
CNN_accu_loss = [] 


for NN in ['MLP', 'CNN']:
   #for p in ['p'+str(19-i) for i in range(20)]:
   for p in ['p18','p19','p16','p17','p14','p15','p12','p13','p10', 'p11', 'p8', 'p9', 'p6','p7','p4','p5','p2', 'p3', 'p0', 'p1']:
        if NN=='MLP':
            accu_file = open('test_fp_vt_error_MLP.'+p+'.log')
            accu_list = accu_file.readlines()
            accu = float(accu_list[1].split()[0].split(':')[1].split('%')[0])/100
            #accu_loss = (95.00 - float(accu)
	    MLP_accu_loss.append(accu) 
        elif NN=='CNN':
            accu_file = open('test_fp_vt_error.'+p+'.log')
            accu_list = accu_file.readlines()
            accu = float(accu_list[1].split()[0].split(':')[1].split('%')[0])/100
            #accu_loss = (98.37 - float(accu))/98.37
	    CNN_accu_loss.append(accu) 
print MLP_accu_loss
print CNN_accu_loss


#initialization
v_t_list = [] 
add_TER_list = []
mul_TER_list = []
v_start = 0.81 
v_end = 0.90
v_step = 0.01 

#define variables 
point_len = 2*((v_end-v_start)/v_step+1.0)
x_list = [i for i in range(point_len+1)]
#print x_list
v_list = [('{0:.2f}'.format(v_start+i/100.0)) for i in range(len(x_list)/2)]
t_list = ['50', '100']
for v in reversed(v_list):
    for t in t_list:
        v_t_list.append('('+v+'V, '+t+degree_sign+')')
print v_t_list
color_list = ['bo-', 'go-']
NN_list = ['MLP','CNN']


plt.xticks(x_list, v_t_list, rotation=30, fontsize=8)
plt.plot(x_list, MLP_accu_loss, color_list[0], label=NN_list[0])
plt.plot(x_list, CNN_accu_loss, color_list[1], label=NN_list[1])
plt.xlabel('Operating condition')
plt.ylabel('Classification Accuracy')
plt.legend(bbox_to_anchor=(0.9, 0.9),bbox_transform=plt.gcf().transFigure)
#plt.show()
plt.savefig('accuracy_v_t.pdf')


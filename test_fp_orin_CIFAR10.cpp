/*10/13/2018 CIFAR-10 test script*/ 
/* Author: Xun Jiao */
#include <cstdlib>
#include <iostream>
#include <vector>
#include "tiny_dnn/tiny_dnn.h"

using namespace tiny_dnn;
using namespace tiny_dnn::activation;
using namespace std;


template <typename N>
void construct_net(N &nn) {
  typedef convolutional_layer<activation::identity> conv;
  typedef max_pooling_layer<relu> pool;

  const int n_fmaps  = 32;  ///< number of feature maps for upper layer
  const int n_fmaps2 = 64;  ///< number of feature maps for lower layer
  const int n_fc     = 64;  ///< number of hidden units in fully-connected layer

  nn << conv(32, 32, 5, 3, n_fmaps, padding::same) << pool(32, 32, n_fmaps, 2)
     << conv(16, 16, 5, n_fmaps, n_fmaps, padding::same)
     << pool(16, 16, n_fmaps, 2)
     << conv(8, 8, 5, n_fmaps, n_fmaps2, padding::same)
     << pool(8, 8, n_fmaps2, 2)
     << fully_connected_layer<activation::identity>(4 * 4 * n_fmaps2, n_fc)
     << fully_connected_layer<softmax>(n_fc, 10);
}

static void test_CIFAR10(const std::string &dictionary, const std::string &data_dir_path) {
  network<sequential> nn;
  construct_net(nn);
  
  //load nets 
  ifstream ifs(dictionary.c_str());
  ifs >> nn;
  //nn.load(dictionary);

  // load CIFAR-10 dataset
  std::vector<label_t> test_labels;
  std::vector<vec_t> test_images;

  parse_cifar10(data_dir_path + "/test_batch.bin", &test_images, &test_labels,
                -1.0, 1.0, 0, 0);
  
  std::cout << "start testing" << std::endl;
  test_images.resize(20); 
  test_labels.resize(20); 
  std::cout << "test image size: " << test_images.size() << std::endl; 
  nn.test(test_images, test_labels).print_detail(std::cout);
  std::cout << "end testing" << std::endl;
}

int main(int argc, char **argv) {
  if (argc != 2) {
    std::cerr << "Usage : " << argv[0] << " path_to_data (example:../data)"
              << std::endl;
    return -1;
  }
  test_CIFAR10("cifar-weights",argv[1]);
  return 0;
}

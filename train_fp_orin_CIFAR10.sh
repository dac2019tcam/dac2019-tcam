g++ -c train_fp_orin_CIFAR10.cpp -I./ -pthread -std=c++11 -DPROFILE=false -DPROF_IMAGE=500 -DCNN_USE_GEMMLOWP=true -DCNN_REGISTER_LAYER_DESERIALIZER=true -DADDER_ERROR_RATE=0 -DMULTI_ERROR_RATE=0 -DTER_ADD=0 -DTER_MUL=0
g++ train_fp_orin_CIFAR10.o -o train_fp_orin_CIFAR10 -pthread -std=c++11 -DPROFILE=false -DPROF_IMAGE=500 -DCNN_USE_GEMMLOWP=true -DCNN_REGISTER_LAYER_DESERIALIZER=true -DADDER_ERROR_RATE=0 -DMULTI_ERROR_RATE=0 -DTER_ADD=0 -DTER_MUL=0
#./train_fp_orin_CIFAR10 examples/cifar10 2>&1 | tee train_fp_orin_CIFAR10.log
./train_fp_orin_CIFAR10 --data_path examples/cifar10 --learning_rate 0.005 --epochs 50 --minibatch_size 128 --backend_type internal | tee train_fp_orin_CIFAR10.log

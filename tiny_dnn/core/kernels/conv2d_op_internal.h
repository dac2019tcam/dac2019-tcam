/*
    COPYRIGHT

    All contributions by Taiga Nomi
    Copyright (c) 2013, Taiga Nomi
    All rights reserved.

    All other contributions:
    Copyright (c) 2013-2016, the respective contributors.
    All rights reserved.

    Each contributor holds copyright over their respective contributions.
    The project versioning (Git) records all such contribution source
   information.

    LICENSE

    The BSD 3-Clause License


    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
   this
      list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.

    * Neither the name of tiny-dnn nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
   LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
   USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma once
#include <bitset>
#include <assert.h>
#include <random>
#include <bitset>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <map>
#include <tuple>

namespace tiny_dnn {
namespace kernels {

//convert float to IEEE 754 string 
inline std::string floatToBinary(float x){
    union
    {
         float input;   // assumes sizeof(float) == sizeof(int)
         int   output;
    }    data;
    data.input = x;
    std::bitset<sizeof(float) * 8> bits(data.output);
    return bits.to_string(); 
}

//layer 1 freq op 
inline std::map<std::tuple<std::string, std::string>, float> BF1(){
    std::map<std::tuple<std::string, std::string>, float> freq_pattern; 
freq_pattern[std::make_tuple("101111100", "101111111")] = 0.1875;
freq_pattern[std::make_tuple("001111100", "101111111")] = -0.1875;
freq_pattern[std::make_tuple("001111011", "101111111")] = -0.09375;
freq_pattern[std::make_tuple("101111101", "101111111")] = 0.375;
freq_pattern[std::make_tuple("001111101", "101111111")] = -0.375;
freq_pattern[std::make_tuple("101111011", "101111111")] = 0.09375;
freq_pattern[std::make_tuple("001111010", "101111111")] = -0.046875;
freq_pattern[std::make_tuple("101111010", "101111111")] = 0.046875;
freq_pattern[std::make_tuple("001111001", "101111111")] = -0.0234375;
freq_pattern[std::make_tuple("101111100", "001111110")] = -0.09375;
freq_pattern[std::make_tuple("101111001", "101111111")] = 0.0234375;
freq_pattern[std::make_tuple("001111100", "001111110")] = 0.09375;
freq_pattern[std::make_tuple("101110110", "101111111")] = 0.0029296875;
freq_pattern[std::make_tuple("001110111", "101111111")] = -0.005859375;
freq_pattern[std::make_tuple("001111000", "101111111")] = -0.01171875;
freq_pattern[std::make_tuple("001111011", "001111110")] = 0.046875;
freq_pattern[std::make_tuple("001111101", "001111110")] = 0.1875;
freq_pattern[std::make_tuple("101111101", "001111110")] = -0.1875;
freq_pattern[std::make_tuple("101111011", "001111110")] = -0.046875;
freq_pattern[std::make_tuple("001111010", "001111110")] = 0.0234375;
freq_pattern[std::make_tuple("101111100", "101111110")] = 0.09375;
freq_pattern[std::make_tuple("101111000", "101111111")] = 0.01171875;
freq_pattern[std::make_tuple("101110111", "101111111")] = 0.005859375;
freq_pattern[std::make_tuple("001111100", "101111110")] = -0.09375;
freq_pattern[std::make_tuple("001111011", "101111110")] = -0.046875;
freq_pattern[std::make_tuple("001111101", "101111110")] = -0.1875;
freq_pattern[std::make_tuple("101111101", "101111110")] = 0.1875;
freq_pattern[std::make_tuple("101111010", "001111110")] = -0.0234375;
freq_pattern[std::make_tuple("101111011", "101111110")] = 0.046875;
freq_pattern[std::make_tuple("001111010", "101111110")] = -0.0234375;
/*
freq_pattern[std::make_tuple("101111100", "001111101")] = -0.046875;
freq_pattern[std::make_tuple("101111100", "101111101")] = 0.046875;
freq_pattern[std::make_tuple("001111001", "001111110")] = 0.01171875;
freq_pattern[std::make_tuple("001111100", "001111101")] = 0.046875;
freq_pattern[std::make_tuple("001111100", "101111101")] = -0.046875;
freq_pattern[std::make_tuple("101111001", "001111110")] = -0.01171875;
freq_pattern[std::make_tuple("001111011", "001111101")] = 0.0234375;
freq_pattern[std::make_tuple("001111011", "101111101")] = -0.0234375;
freq_pattern[std::make_tuple("101111010", "101111110")] = 0.0234375;
freq_pattern[std::make_tuple("001111101", "001111101")] = 0.09375;
freq_pattern[std::make_tuple("101111101", "001111101")] = -0.09375;
freq_pattern[std::make_tuple("001111101", "101111101")] = -0.09375;
freq_pattern[std::make_tuple("101111101", "101111101")] = 0.09375;
freq_pattern[std::make_tuple("101111011", "001111101")] = -0.0234375;
freq_pattern[std::make_tuple("101111011", "101111101")] = 0.0234375;
freq_pattern[std::make_tuple("101110110", "001111110")] = -0.00146484375;
freq_pattern[std::make_tuple("001111000", "001111110")] = 0.005859375;
freq_pattern[std::make_tuple("001110111", "001111110")] = 0.0029296875;
freq_pattern[std::make_tuple("101111100", "001111111")] = -0.1875;
freq_pattern[std::make_tuple("001111010", "001111101")] = 0.01171875;
*/
    return freq_pattern; 
}

//layer 2 freq op
inline std::map<std::tuple<std::string, std::string>, float> BF2(){
    std::map<std::tuple<std::string, std::string>, float> freq_pattern; 
freq_pattern[std::make_tuple("001111011", "101111101")] = -0.0234375;
freq_pattern[std::make_tuple("001111100", "101111101")] = -0.046875;
freq_pattern[std::make_tuple("001111011", "001111101")] = 0.0234375;
freq_pattern[std::make_tuple("101111100", "101111101")] = 0.046875;
freq_pattern[std::make_tuple("101111011", "101111101")] = 0.0234375;
freq_pattern[std::make_tuple("001111100", "001111101")] = 0.046875;
freq_pattern[std::make_tuple("101111100", "101111110")] = 0.09375;
freq_pattern[std::make_tuple("001111100", "101111110")] = -0.09375;
freq_pattern[std::make_tuple("101111011", "001111101")] = -0.0234375;
freq_pattern[std::make_tuple("101111100", "001111101")] = -0.046875;
freq_pattern[std::make_tuple("101111010", "101111110")] = 0.0234375;
freq_pattern[std::make_tuple("101111011", "101111110")] = 0.046875;
freq_pattern[std::make_tuple("101111010", "101111101")] = 0.01171875;
freq_pattern[std::make_tuple("001111101", "101111101")] = -0.09375;
freq_pattern[std::make_tuple("001111010", "101111101")] = -0.01171875;
freq_pattern[std::make_tuple("001111011", "001111100")] = 0.01171875;
freq_pattern[std::make_tuple("001111011", "101111110")] = -0.046875;
freq_pattern[std::make_tuple("101111010", "001111101")] = -0.01171875;
freq_pattern[std::make_tuple("001111100", "101111100")] = -0.0234375;
freq_pattern[std::make_tuple("001111010", "001111101")] = 0.01171875;
freq_pattern[std::make_tuple("001111011", "101111100")] = -0.01171875;
freq_pattern[std::make_tuple("001111100", "001111100")] = 0.0234375;
freq_pattern[std::make_tuple("101111100", "101111100")] = 0.0234375;
freq_pattern[std::make_tuple("101111011", "001111100")] = -0.01171875;
freq_pattern[std::make_tuple("101111100", "001111110")] = -0.09375;
freq_pattern[std::make_tuple("101111100", "001111100")] = -0.0234375;
freq_pattern[std::make_tuple("001111100", "001111110")] = 0.09375;
freq_pattern[std::make_tuple("101111011", "101111100")] = 0.01171875;
freq_pattern[std::make_tuple("001111001", "101111101")] = -0.005859375;
freq_pattern[std::make_tuple("001111001", "001111101")] = 0.005859375;
/*
freq_pattern[std::make_tuple("001111011", "001111110")] = 0.046875;
freq_pattern[std::make_tuple("001111101", "101111110")] = -0.1875;
freq_pattern[std::make_tuple("001111010", "101111110")] = -0.0234375;
freq_pattern[std::make_tuple("101111011", "001111110")] = -0.046875;
freq_pattern[std::make_tuple("101111101", "101111110")] = 0.1875;
freq_pattern[std::make_tuple("101111001", "101111101")] = 0.005859375;
freq_pattern[std::make_tuple("001111101", "101111100")] = -0.046875;
freq_pattern[std::make_tuple("101111010", "001111110")] = -0.0234375;
freq_pattern[std::make_tuple("001111101", "001111101")] = 0.09375;
freq_pattern[std::make_tuple("101111101", "101111101")] = 0.09375;
freq_pattern[std::make_tuple("001111101", "001111100")] = 0.046875;
freq_pattern[std::make_tuple("001111001", "101111110")] = -0.01171875;
freq_pattern[std::make_tuple("101111010", "101111100")] = 0.005859375;
freq_pattern[std::make_tuple("001111010", "101111100")] = -0.005859375;
freq_pattern[std::make_tuple("001111101", "001111110")] = 0.1875;
freq_pattern[std::make_tuple("101111001", "001111101")] = -0.005859375;
freq_pattern[std::make_tuple("001111001", "001111100")] = 0.0029296875;
freq_pattern[std::make_tuple("001111010", "001111100")] = 0.005859375;
freq_pattern[std::make_tuple("001111001", "101111100")] = -0.0029296875;
freq_pattern[std::make_tuple("101111010", "001111100")] = -0.005859375;
*/
    return freq_pattern; 
}

//layer 3 freq op
inline std::map<std::tuple<std::string, std::string>, float> BF3(){
    std::map<std::tuple<std::string, std::string>, float> freq_pattern; 
freq_pattern[std::make_tuple("001111011", "001111101")] = 0.0234375;
freq_pattern[std::make_tuple("101111011", "001111101")] = -0.0234375;
freq_pattern[std::make_tuple("101111011", "101111101")] = 0.0234375;
freq_pattern[std::make_tuple("001111011", "101111101")] = -0.0234375;
freq_pattern[std::make_tuple("001111011", "001111110")] = 0.046875;
freq_pattern[std::make_tuple("101111011", "001111110")] = -0.046875;
freq_pattern[std::make_tuple("101111011", "101111110")] = 0.046875;
freq_pattern[std::make_tuple("101111010", "001111101")] = -0.01171875;
freq_pattern[std::make_tuple("001111010", "001111101")] = 0.01171875;
freq_pattern[std::make_tuple("001111011", "101111110")] = -0.046875;
freq_pattern[std::make_tuple("101111011", "101111100")] = 0.01171875;
freq_pattern[std::make_tuple("101111011", "001111100")] = -0.01171875;
freq_pattern[std::make_tuple("001111011", "101111100")] = -0.01171875;
freq_pattern[std::make_tuple("001111011", "001111100")] = 0.01171875;
freq_pattern[std::make_tuple("101111010", "101111101")] = 0.01171875;
freq_pattern[std::make_tuple("001111010", "101111101")] = -0.01171875;
freq_pattern[std::make_tuple("101111010", "001111110")] = -0.0234375;
freq_pattern[std::make_tuple("001111010", "001111110")] = 0.0234375;
freq_pattern[std::make_tuple("101111010", "101111100")] = 0.005859375;
freq_pattern[std::make_tuple("001111010", "101111100")] = -0.005859375;
freq_pattern[std::make_tuple("101111100", "101111110")] = 0.09375;
freq_pattern[std::make_tuple("101111011", "001111011")] = -0.005859375;
freq_pattern[std::make_tuple("101111011", "101111011")] = 0.005859375;
freq_pattern[std::make_tuple("001111010", "001111100")] = 0.005859375;
freq_pattern[std::make_tuple("101111100", "101111101")] = 0.046875;
freq_pattern[std::make_tuple("001111011", "001111011")] = 0.005859375;
freq_pattern[std::make_tuple("101111010", "001111100")] = -0.005859375;
freq_pattern[std::make_tuple("101111100", "001111101")] = -0.046875;
freq_pattern[std::make_tuple("001111100", "001111101")] = 0.046875;
freq_pattern[std::make_tuple("101111010", "101111110")] = 0.0234375;
/*
freq_pattern[std::make_tuple("001111100", "101111101")] = -0.046875;
freq_pattern[std::make_tuple("001111010", "101111110")] = -0.0234375;
freq_pattern[std::make_tuple("001111011", "101111011")] = -0.005859375;
freq_pattern[std::make_tuple("001111100", "101111110")] = -0.09375;
freq_pattern[std::make_tuple("101111100", "001111110")] = -0.09375;
freq_pattern[std::make_tuple("001111100", "001111110")] = 0.09375;
freq_pattern[std::make_tuple("001111010", "001111011")] = 0.0029296875;
freq_pattern[std::make_tuple("101111010", "101111011")] = 0.0029296875;
freq_pattern[std::make_tuple("001111010", "101111011")] = -0.0029296875;
freq_pattern[std::make_tuple("001111001", "001111101")] = 0.005859375;
freq_pattern[std::make_tuple("101111010", "001111011")] = -0.0029296875;
freq_pattern[std::make_tuple("101111011", "001111010")] = -0.0029296875;
freq_pattern[std::make_tuple("101111001", "001111101")] = -0.005859375;
freq_pattern[std::make_tuple("001111001", "101111101")] = -0.005859375;
freq_pattern[std::make_tuple("101111100", "101111100")] = 0.0234375;
freq_pattern[std::make_tuple("001111100", "101111100")] = -0.0234375;
freq_pattern[std::make_tuple("001111011", "001111010")] = 0.0029296875;
freq_pattern[std::make_tuple("101111001", "101111101")] = 0.005859375;
freq_pattern[std::make_tuple("101111100", "001111100")] = -0.0234375;
freq_pattern[std::make_tuple("101111010", "001111010")] = -0.00146484375;
*/
    return freq_pattern; 
}

//approximate bloom filter hit 
int hit = 0; 
int total = 0; 
inline float check_BV(float op1, float op2, int layer) {
    total ++; 
    float temp_mul; 
    std::map<std::tuple<std::string, std::string>, float> freq_pattern;
    if (layer == 1) 
       freq_pattern = BF1(); 
    else if (layer == 2)
       freq_pattern = BF2(); 
    else 
       freq_pattern = BF3(); 
    std::string op1_str = floatToBinary(op1);
    std::string op2_str = floatToBinary(op2);
    //std::cout << "op1: " << op1 << " " << op1_str.substr(0,13) << std::endl; 
    //std::cout << "op2: " << op2 << " " << op2_str.substr(0,13) << std::endl; 
    if (freq_pattern.find(std::make_tuple(op1_str.substr(0,9), op2_str.substr(0,9))) != freq_pattern.end())
    {
       hit ++; 
       temp_mul = freq_pattern[std::make_tuple(op1_str.substr(0,9), op2_str.substr(0,9))];
       //std::cout << "Hit " << op1 << " * " << op2 << " = " << temp_mul << std::endl; 
       return temp_mul; 
    }  
    else
    {
       double decision = (double)rand() / (double)RAND_MAX; 
       if(decision < 0.001)
       {
          switch(layer) {
             case 1: temp_mul = 0.1875; break; 
             case 2: temp_mul = -0.0234375; break; 
             case 0: temp_mul = 0.0234375; break;
          }
          //std::cout << "decision is " << decision << ", temp_mul is " << temp_mul << std::endl;   
       }
       else 
          temp_mul = op1 * op2; 
       return temp_mul; 
    }
}

int conv_count = 0; 
inline void conv2d_op_internal(const tensor_t &in_data,
                               const vec_t &W,
                               const vec_t &bias,
                               tensor_t &out_data,
                               const core::conv_params &params,
                               const bool parallelize) {
  conv_count ++; 
  std::cout << "conv count: " << conv_count << std::endl; 
  std::cout << "total op: " << total << std::endl; 
  std::cout << "hit op: "   << hit << std::endl; 
  //initialize profile file 
  FILE *op_MUL_file1 = fopen("./operands/conv2d_MUL_op_layer1_CNN","a");
  FILE *op_MUL_file2 = fopen("./operands/conv2d_MUL_op_layer2_CNN","a");
  FILE *op_MUL_file3 = fopen("./operands/conv2d_MUL_op_layer3_CNN","a");
  float temp_mul; 
  for_i(parallelize, in_data.size(), [&](int sample) {
    const vec_t &in = in_data[sample];
    vec_t &a        = out_data[sample];

    for (serial_size_t o = 0; o < params.out.depth_; o++) {
      for (serial_size_t inc = 0; inc < params.in.depth_; inc++) {
        if (!params.tbl.is_connected(o, inc)) continue;

        serial_size_t idx = 0;
        idx               = params.in.depth_ * o + inc;
        idx               = params.weight.get_index(0, 0, idx);
        const float_t *pw = &W[idx];

        idx               = params.in_padded.get_index(0, 0, inc);
        const float_t *pi = &in[idx];

        idx         = params.out.get_index(0, 0, o);
        float_t *pa = &a[idx];

        for (serial_size_t y = 0; y < params.out.height_; y++) {
          for (serial_size_t x = 0; x < params.out.width_; x++) {
            const float_t *ppw = pw;
            const float_t *ppi =
              pi + params.in_padded.width_ * (y * params.h_stride) +
              x * params.w_stride;
            float_t sum{0};

            // should be optimized for small kernel(3x3,5x5)
            for (serial_size_t wy = 0; wy < params.weight.height_;
                 wy++) {  // NOLINT
              for (serial_size_t wx = 0; wx < params.weight.width_;
                   wx++) {  // NOLINT
                idx = wy * params.in_padded.width_ + wx;
                float temp_ppw = *ppw++;   //add a temp variable for pointer-based value
                //sum += temp_ppw * ppi[idx];                         //use exact multiplication                       
                float BV_res = check_BV(temp_ppw, ppi[idx], conv_count % 3);
                sum += BV_res;  //use bloom filter 
                //sum += *ppw++ * ppi[idx];   //original formula 
              
                //profile MUL operands; Xun 09/06/17 
                if (PROFILE == true)
                {
                    //profile layer-based operands; Xun 09/06/17
                    if (conv_count < PROF_IMAGE * 3)   //only profile PROF_IMAGE images
                    {
                        if (conv_count % 3 == 1)
                           fprintf(op_MUL_file1, "%f\t%f\n", temp_ppw, ppi[idx]);
                        if (conv_count % 3 == 2)
                           fprintf(op_MUL_file2, "%f\t%f\n", temp_ppw, ppi[idx]);
                        if (conv_count % 3 == 0)
                           fprintf(op_MUL_file3, "%f\t%f\n", temp_ppw, ppi[idx]);
                    }
                }
                
                //float temp_mul = (int)((*ppw++ * ppi[idx])/0.01) * 0.01;     //round to nearest one-hundredth
                //sum += temp_mul;
              }
            }
            pa[y * params.out.width_ + x] += sum;
          }
        }
      }

      if (params.has_bias) {
        float_t *pa  = &a[params.out.get_index(0, 0, o)];
        float_t *paa = pa + params.out.width_ * params.out.height_;
        std::for_each(pa, paa, [&](float_t &f) { f += bias[o]; });
      }
    }
  });
  fclose(op_MUL_file1);
  fclose(op_MUL_file2);
  fclose(op_MUL_file3);
}

/******************************************************************/

template <typename tensor_t, typename vec_t>
void conv2d_op_internal(const tensor_t &prev_out,
                        const vec_t &W,
                        tensor_t &dW,
                        tensor_t &db,
                        tensor_t &curr_delta,
                        tensor_t &prev_delta,
                        const core::conv_params &params,
                        const bool parallelize) {
  typedef typename vec_t::value_type float_t;

  for_i(parallelize, prev_out.size(), [&](int sample) {
    // propagate delta to previous layer
    for (serial_size_t inc = 0; inc < params.in.depth_; inc++) {
      for (serial_size_t outc = 0; outc < params.out.depth_; outc++) {
        if (!params.tbl.is_connected(outc, inc)) continue;

        serial_size_t idx = 0;
        idx               = params.in.depth_ * outc + inc;
        idx               = params.weight.get_index(0, 0, idx);
        const float_t *pw = &W[idx];

        idx                       = params.out.get_index(0, 0, outc);
        const float_t *pdelta_src = &curr_delta[sample][idx];

        idx = params.in_padded.get_index(0, 0, inc);
        // float_t* pdelta_dst = &(*prev_delta)[sample][idx];
        float_t *pdelta_dst = &prev_delta[sample][idx];

        for (serial_size_t y = 0; y < params.out.height_; y++) {
          for (serial_size_t x = 0; x < params.out.width_; x++) {
            const float_t *ppw = pw;

            idx                       = y * params.out.width_ + x;
            const float_t ppdelta_src = pdelta_src[idx];

            float_t *ppdelta_dst =
              pdelta_dst + y * params.h_stride * params.in_padded.width_ +
              x * params.w_stride;

            for (serial_size_t wy = 0; wy < params.weight.height_;
                 wy++) {  // NOLINT
              for (serial_size_t wx = 0; wx < params.weight.width_;
                   wx++) {  // NOLINT
                idx = wy * params.in_padded.width_ + wx;
                ppdelta_dst[idx] += *ppw++ * ppdelta_src;
              }
            }
          }
        }
      }
    }

    // accumulate dw
    for (serial_size_t inc = 0; inc < params.in.depth_; inc++) {
      for (serial_size_t outc = 0; outc < params.out.depth_; outc++) {
        if (!params.tbl.is_connected(outc, inc)) continue;

        for (serial_size_t wy = 0; wy < params.weight.height_; wy++) {
          for (serial_size_t wx = 0; wx < params.weight.width_; wx++) {
            float_t dst{0};

            serial_size_t idx    = 0;
            idx                  = params.in_padded.get_index(wx, wy, inc);
            const float_t *prevo = &prev_out[sample][idx];

            idx                  = params.out.get_index(0, 0, outc);
            const float_t *delta = &curr_delta[sample][idx];

            if (params.w_stride > 1) {
              for (serial_size_t y = 0; y < params.out.height_; y++) {
                serial_size_t prevo_idx =
                  y * params.in_padded.width_ * params.h_stride;
                serial_size_t delta_idx = y * params.out.width_;

                for (serial_size_t x = 0; x < params.out.width_; x++) {
                  dst += prevo[prevo_idx + x * params.w_stride] *
                         delta[delta_idx + x];
                }
              }
            } else {
              for (serial_size_t y = 0; y < params.out.height_; y++) {
                dst += vectorize::dot(
                  prevo + y * params.in_padded.width_ * params.h_stride,
                  delta + y * params.out.width_, params.out.width_);
              }
            }

            idx = params.in.depth_ * outc + inc;
            dW[sample][params.weight.get_index(wx, wy, idx)] += dst;
          }
        }
      }
    }

    // accumulate db
    if (params.has_bias) {
      for (serial_size_t outc = 0; outc < params.out.depth_; outc++) {
        serial_size_t idx     = params.out.get_index(0, 0, outc);
        const float_t *delta  = &curr_delta[sample][idx];
        const float_t *deltaa = delta + params.out.width_ * params.out.height_;
        db[sample][outc] += std::accumulate(delta, deltaa, float_t{0});
      }
    }
  });
}

}  // namespace kernels
}  // namespace tiny_dnn
